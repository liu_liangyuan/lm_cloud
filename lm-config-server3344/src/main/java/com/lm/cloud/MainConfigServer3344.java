package com.lm.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * enable config server 表示这个项目是一个config-center server
 */
@SpringBootApplication
@EnableConfigServer
public class MainConfigServer3344 {
    public static void main(String[] args){
        SpringApplication.run(MainConfigServer3344.class, args);
    }
}
