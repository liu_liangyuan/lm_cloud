package com.lm.cloud.service;

import org.springframework.messaging.Message;

public interface IReceiveMessage {
    public void receiveMessage(Message<String> message);
}
