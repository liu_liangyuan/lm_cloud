package com.lm.cloud.service.fallback;

import com.lm.cloud.service.IOrderService;
import org.springframework.stereotype.Component;

@Component
public class OrderFallback implements IOrderService {
    @Override
    public String testStraightway() {
        return "A error from testStraightway";
    }

    @Override
    public String testDelay() {
        return "A error from testDelay";
    }

    @Override
    public String testException() {
        return "A error from testException";
    }
}
