package com.lm.cloud.service;


import com.lm.cloud.service.fallback.OrderFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(value = "HYSTRIX-PROVIDER-PAYMENT", fallback = OrderFallback.class)
public interface IOrderService {
    @GetMapping("/payment/test/Immediately")
    public String testStraightway();

    @GetMapping("/payment/test/delay")
    public String testDelay();

    @GetMapping("/payment/test/exception")
    public String testException();
}
