package com.lm.cloud.controller;

import com.lm.cloud.service.IOrderService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class OrderController {

    @Resource
    IOrderService orderService;

    @GetMapping("/order/test/straightway")
    public String test1() {
        return orderService.testStraightway();
    }

    @GetMapping("/order/test/delay")
    public String test2() {
        String s = orderService.testDelay();
        log.info("---------------------------------------" + s + "------------------------------------------");
        return s;
    }

    @HystrixCommand(
            fallbackMethod = "paymentTimeOutFallbackMethod",
            commandProperties = {
                    @HystrixProperty(
                            name = "execution.isolation.thread.timeoutInMilliseconds",
                            value = "3000"
                    )
            })
    @GetMapping("/order/timeout")
    public String testTimeout() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "success";
    }

    public String paymentTimeOutFallbackMethod() {
        return "An error occurred";
    }
}
