package com.lm.cloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfigure {

    /**
     * route 是用来构建自己路由的
     * r -> r.path("/guonei").uri("http://news.baidu.com/guonei")) 表示将9527端口的guonei路径映射到http://news.baidu.com/guonei
     *
     * @param routeLocatorBuilder 路由构建器
     * @return 返回一个路由构建
     */
    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder routeLocatorBuilder) {
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("payment_route", r -> r.path("/guonei").uri("http://news.baidu.com/guonei")).build();
        return routes.build();
    }
}
