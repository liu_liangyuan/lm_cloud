package com.lm.cloud.service;

public interface IMessageProvider {
    public String send();
}
