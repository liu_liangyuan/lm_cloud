package com.lm.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MainSender8801 {
    public static void main(String[] args){
        SpringApplication.run(MainSender8801.class, args);
    }
}
