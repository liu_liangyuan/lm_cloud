package com.lm.cloud.service.impl;

import cn.hutool.core.util.IdUtil;
import com.lm.cloud.service.IPaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@DefaultProperties(defaultFallback = "paymentTimeOutFallbackMethod")
public class PaymentService implements IPaymentService {
    @Override
    public String respondImmediately() {
        String threatName = Thread.currentThread().getName();
        String uuid = IdUtil.randomUUID().replace("-", "");
        return "respond Immediately --- " + threatName + "---" + uuid + "立即执行";
    }

    /**
     * 娄底方法是responseDelayed_fallback
     * timeoutInMilliseconds表示时长限制
     *
     * @return
     */
//    @HystrixCommand(
//            fallbackMethod = "paymentTimeOutFallbackMethod",
//            commandProperties = {
//                    @HystrixProperty(
//                            name = "execution.isolation.thread.timeoutInMilliseconds",
//                            value = "3000"
//                    )
//            })
    @HystrixCommand
    public String responseException() {
        int t = 10 / 0;
        return "response Delayed --- 异常处理";
    }

    @HystrixCommand(
            fallbackMethod = "paymentTimeOutFallbackMethod",
            commandProperties = {
                    @HystrixProperty(
                            name = "execution.isolation.thread.timeoutInMilliseconds",
                            value = "3000"
                    )
            })
    public String responseDelayed() {
        String threatName = Thread.currentThread().getName();
        String uuid = IdUtil.randomUUID().replace("-", "");
        int timeNumber = 3;
        try {
            TimeUnit.SECONDS.sleep(timeNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "response Delayed --- " + threatName + "---" + uuid + "延迟" + timeNumber + "秒";
    }

    /**
     * 这是responseDelayed方法的娄底犯法
     */
    public String paymentTimeOutFallbackMethod() {
        return "怎么回事呢？出错了！！！！";
    }


    //服务熔断
    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_fallback",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),  //是否开启断路器
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),   //请求次数
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"),  //时间范围
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "60"), //失败率达到多少后跳闸
    })
    public String paymentCircuitBreaker(@PathVariable("id") Integer id){
        if (id < 0){
            throw new RuntimeException("*****id 不能负数");
        }
        String serialNumber = IdUtil.simpleUUID();

        return Thread.currentThread().getName()+"\t"+"调用成功,流水号："+serialNumber;
    }
    public String paymentCircuitBreaker_fallback(@PathVariable("id") Integer id){
        return "id 不能负数，请稍候再试,(┬＿┬)/~~     id: " +id;
    }
}
