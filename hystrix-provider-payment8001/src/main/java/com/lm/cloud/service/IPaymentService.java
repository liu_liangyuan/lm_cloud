package com.lm.cloud.service;

public interface IPaymentService {

    public String respondImmediately();// 立即响应

    public String responseDelayed();// 延迟响应

    public String responseException();

    public String paymentCircuitBreaker(Integer id);

    public String paymentCircuitBreaker_fallback(Integer id);
}
