package com.lm.cloud.controller;

import com.lm.cloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    IPaymentService paymentService;

    @GetMapping("/payment/test/Immediately")
    public String testStraightway() {
        return paymentService.respondImmediately();
    }

    @GetMapping("/payment/test/exception")
    public String testException() {
        return paymentService.responseException();
    }

    @GetMapping("/payment/test/timeout")
    public String testDelay() {
        return paymentService.responseDelayed();
    }

    //===服务熔断
    @GetMapping("/payment/circuit/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id){
        String result = paymentService.paymentCircuitBreaker(id);
        log.info("*******result:"+result);
        return result;
    }
}
