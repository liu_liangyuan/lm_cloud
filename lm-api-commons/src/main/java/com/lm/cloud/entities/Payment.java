package com.lm.cloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author  良木
 * @date 2021-01-02 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Payment  implements Serializable {

	private static final long serialVersionUID =  3124307705335463252L;

	private Long id;

	private String serial;

	public Payment(String str){
		this.serial = str;
	}

}
