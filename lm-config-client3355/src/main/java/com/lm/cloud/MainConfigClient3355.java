package com.lm.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainConfigClient3355 {
    public static void main(String[] args) {
        SpringApplication.run(MainConfigClient3355.class, args);
    }
}
