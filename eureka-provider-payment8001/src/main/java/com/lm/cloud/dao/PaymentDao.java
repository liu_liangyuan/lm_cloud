package com.lm.cloud.dao;

import org.apache.ibatis.annotations.Mapper;
import com.lm.cloud.entities.Payment;

@Mapper
public interface PaymentDao {

    public int create(Payment payment);

    public Payment getPaymentById(Long id);
}
