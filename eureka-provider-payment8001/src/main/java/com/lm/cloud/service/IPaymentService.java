package com.lm.cloud.service;


import com.lm.cloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

public interface IPaymentService {

    int create(Payment payment); //写

    Payment getPaymentById(@Param("id") Long id);  //读取
}
 
 
