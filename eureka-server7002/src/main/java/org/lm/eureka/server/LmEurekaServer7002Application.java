package org.lm.eureka.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class LmEurekaServer7002Application {

    public static void main(String[] args) {
        SpringApplication.run(LmEurekaServer7002Application.class, args);
    }

}
