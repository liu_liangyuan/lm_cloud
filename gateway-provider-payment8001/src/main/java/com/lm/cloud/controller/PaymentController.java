package com.lm.cloud.controller;

import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PaymentController {

    @Value("${server.port}")
    private String SERVER_PORT;

    @GetMapping("/payment/get/{id}")
    public String testGet(@PathVariable("id") Integer id) {
        String uuid = IdUtil.simpleUUID();
        log.info(uuid + ": your input the ID is " + id + "||server_port = " + SERVER_PORT);
        return uuid + ": your input the ID is " + id + "||server_port = " + SERVER_PORT;
    }

    @GetMapping("/payment/lb")
    public String testLb() {
        String uuid = IdUtil.simpleUUID();
        log.info(uuid + ": lb||server_port = " + SERVER_PORT);
        return uuid + ": lb||server_port = " + SERVER_PORT;
    }
}
