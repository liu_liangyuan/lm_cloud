package com.lm.cloud.service.impl;

import com.lm.cloud.dao.PaymentDao;
import com.lm.cloud.entities.Payment;
import com.lm.cloud.service.IPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService implements IPaymentService {

    @Autowired
    PaymentDao paymentDao;

    @Override
    public Payment getById(Integer id) {
        Payment payment = paymentDao.selectById(id);
        return payment;
    }

    @Override
    public int update(Payment payment) {
        int id = paymentDao.updateById(payment);
        return id;
    }
}
