package com.lm.cloud.service;

import com.lm.cloud.entities.Payment;

public interface IPaymentService {
    public Payment getById(Integer id);
    public int update(Payment payment);
}
