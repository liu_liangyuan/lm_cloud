package com.lm.cloud.controller;

import com.lm.cloud.entities.CommonResult;
import com.lm.cloud.entities.Payment;
import com.lm.cloud.service.IPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class PaymentController {

    @Resource
    private IPaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getById(@PathVariable("id") Integer id) {
        Payment payment = paymentService.getById(id);
        if (payment == null) {
            log.error("payment does not exist");
            return new CommonResult<Payment>(500, "payment don't exist");
        }
        return new CommonResult<>(200, "OK, from the port :" + serverPort, payment);
    }

    @GetMapping("/payment/update")
    public CommonResult<Payment> updatePayment(
            @RequestParam("id") Long id,
            @RequestParam("ser") String str
    ) {
        Payment payment = new Payment(id, str);
        int pid = paymentService.update(payment);
        Payment p = paymentService.getById(pid);
        if (p.equals(payment)) {
            return new CommonResult<>(200, "OK", payment);
        }
        return new CommonResult<>(500, "update false");
    }
}
