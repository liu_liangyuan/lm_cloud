package com.lm.cloud.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lm.cloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaymentDao extends BaseMapper<Payment> {}
