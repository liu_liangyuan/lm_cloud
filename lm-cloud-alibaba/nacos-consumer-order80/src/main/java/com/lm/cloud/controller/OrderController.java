package com.lm.cloud.controller;

import com.lm.cloud.entities.CommonResult;
import com.lm.cloud.entities.Payment;
import com.lm.cloud.service.PaymentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderController {

    @Resource
    private PaymentService paymentService;

    @GetMapping("/order/pay/{id}")
    public CommonResult<Payment> pay(@PathVariable("id") Integer id) {
        CommonResult<Payment> result = paymentService.getById(id);
        return result;
    }

    @GetMapping("/order/update/{serial}")
    public CommonResult<Payment> update(@PathVariable("serial") String serial) {
        CommonResult<Payment> result = paymentService.updatePayment(1l, serial);
        return result;
    }
}
