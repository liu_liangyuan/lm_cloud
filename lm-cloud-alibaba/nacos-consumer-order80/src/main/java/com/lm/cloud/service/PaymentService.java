package com.lm.cloud.service;

import com.lm.cloud.entities.CommonResult;
import com.lm.cloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Component
@FeignClient(value = "payment-provider")
public interface PaymentService {

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getById(@PathVariable("id") Integer id);

    @GetMapping("/payment/update")
    public CommonResult<Payment> updatePayment(
            @RequestParam("id") Long id,
            @RequestParam("ser") String str
    );
}
