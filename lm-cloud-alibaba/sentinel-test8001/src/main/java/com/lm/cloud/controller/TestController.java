package com.lm.cloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController
public class TestController {

    /**
     * 一般url访问测试
     *
     * @return
     */
    @GetMapping("/test1")
    public String test1() {
        return "from the test1";
    }

    /**
     * 超时测试
     *
     * @return
     */
    @GetMapping("/test2")
    public String test2() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "from the test2, sleep 3s";
    }

    /**
     * 异常情况下测试，测试结果和hystrix对比后，发现器直接给出异常信息，而不是采用sentinel的fallback策略
     *
     * @return
     */
    @GetMapping("/test3")
    public String test3() {

        int i = 10 / 0; // 报异常
        return "success";
    }

    /**
     * hot key 测试
     *
     * @return
     */
    @GetMapping("/test4")
    public String test4(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "address", required = false) String address) {
        return "from the test4, name:" + name + "address" + address;

    }

    /**
     * hot key 测试
     *
     * @return
     */
    @SentinelResource()
    @GetMapping("/test5")
    public String test5() {
        return "from the test5";

    }
}