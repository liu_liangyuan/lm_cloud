package com.lm.cloud.controller;

import com.lm.cloud.dao.PaymentDao;
import com.lm.cloud.entities.CommonResult;
import com.lm.cloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RefreshScope
public class PaymentController {

    @Autowired
    PaymentDao paymentDao;

    @Value("${config.info}")
    String serverVersion;

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Integer id) {
        Payment payment = paymentDao.selectById(id);
        if (payment != null) {
            return new CommonResult<>(200, "ok", payment);
        }
        return new CommonResult<>(500, "not found");
    }

    @GetMapping("/payment/server/version")
    public String getVersion() {
        return "payment‘s version is " + serverVersion;
    }
}
