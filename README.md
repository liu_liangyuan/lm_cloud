# lm_cloud

#### 介绍
spring cloud的一个整合项目，主要用来快速配置与查看

#### 软件架构

spring cloud的所有整合，包括：
1. 服务注册：zookeeper consul eureka nacos
2. 服务调用：ribbon loadBalancer feign openfeign
3. 服务降级：hystrix resilience4j sentienl
4. 服务网关：zuul zuul2 gateway
5. 服务配置：config nacos
6. 服务总线：bus nacos
#### 安装教程

下载源码直接套用就OK了，每个套用都是以服务名开头，例如zookeeper-provider-payment8081，表示zookeeper的一个项目整合

#### 使用说明

项目提取即用，完全遵循：约定>配置>code

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
